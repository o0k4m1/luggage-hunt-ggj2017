﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Laner : MonoBehaviour {

	public static Laner Instance { get; private set; }


	Transform[] lanes;
	public Transform laneLimit;

	public void MoveLanesBy(float x) {
		for(int i = 0; i < lanes.Length; i++) {
			var lane = lanes[i];
			if(lane.position.x > laneLimit.position.x) {
				var nextLane = lanes[(i + 1) % lanes.Length];
				lane.position = nextLane.position - new Vector3(lane.GetComponent<SpriteRenderer>().sprite.bounds.extents.x * 2, 0f, 0f);
				break;
			}
		}
		for(int i = 0; i < lanes.Length; i++) {
			var lane = lanes[i];
			lane.Translate(new Vector3(x, 0f, 0f));
		}
	}

	void Awake() {
		lanes = GetComponentsInChildren<SpriteRenderer>().Select(sr => sr.transform).ToArray();
		Instance = this;
	}
	void OnDestroy() {
		Instance = null;
	}
}
