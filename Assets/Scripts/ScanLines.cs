﻿using UnityEngine;

public class ScanLines : MonoBehaviour {
	private Rigidbody2D rg;
	private Vector3 startPos;

	// Use this for initialization
	void Start () {
		this.rg = gameObject.GetComponent<Rigidbody2D>();
		this.rg.isKinematic = true;
		this.startPos = gameObject.transform.position;
	}
	
	void FixedUpdate () {
		if(!this.rg.isKinematic)
		{
			this.rg.AddForce(Vector2.down * 0.05f, ForceMode2D.Impulse);
		}
	}

	public void Drop()
	{
		this.rg.isKinematic = false;
		SoundManager._instance.PlaySfx(SoundManager._instance.scan);
	}

	void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "ScannerCollider")
        {
			Debug.Log("destroy scanlines!");
			
			var newScanLines = Resources.Load("scanlines") as GameObject;
			Instantiate(newScanLines);
			newScanLines.transform.position = this.startPos;

			GameManager.instance.InitTimer();

			Destroy(gameObject);
		}
    }
}
