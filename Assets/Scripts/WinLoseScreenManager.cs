﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinLoseScreenManager : MonoBehaviour {

	string mainSceneName = "mainscene";
	string mainMenuSceneName = "mainMenu";
	public string scoreTemplate = "Score accumulated:\n{0}";
	public Text scoreField;

	void Start() {
		if(scoreField != null) {
			scoreField.text = scoreTemplate.Format(GameManager.score);
		}
	}

	public void ReplayButtonPressed() {
		// just in case normalize timescale
		Time.timeScale = 1f;
		SceneManager.LoadScene(mainSceneName);
	}

	public void MenuButtonPressed() {
		// just in case normalize timescale
		Time.timeScale = 1f;
		// instantiate menu ? or to go main menu
		SceneManager.LoadScene(mainMenuSceneName);
	}

	public void StartGameButtonPressed() {
		SceneManager.LoadScene(mainSceneName);
	}

	public void Exit()
	{
		Application.Quit();
	}
}
