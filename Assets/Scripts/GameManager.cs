﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;              // Static instance of GameManager which allows it to be accessed by any other script.

    public UIManager UI;
    
    private Collider2D inspectCol;
    private Collider2D reviewCol;

    public bool Moving;                                     // ar musu lagaminai juda
    public bool Inspection;                                 // ar darom apziura
    public bool Review;                                     // ar parodom atsakyma
    public bool Timer;                                      // ar timeris?


    [Space(15f)]
    public string winSceneName = "winScene";
    public string loseSceneName = "loseScene";

    private bool? currentResult;
    private bool currentTimeout;

    public static int score = 0;

    public SpriteRenderer Lamp;
    public Light LampLight;
    bool LampLightOn = false;

    private string[] GoodPossibleItems = new string[17] {
		"pizza",
		"banana",
		"mirror",
		"sock",
		"perfume",
		"cactus",
		"keys",
        "bra",
        "comb",
        "fan",
        "glasses",
        "headset",
        "underwear",
        "water",
        "wig",
        "watergun",
        "cup"
    };
    private string[] BadPossibleItems = new string[7] {
        "knife",
        "hand",
        "scissors",
        "syringe",
        "brain",
        "dynamite",
        "snake"
    };


        
    private string[] PossibleSuitcases = new string[6] {
		"suitcase yellow",
		"suitcase blue",
		"suitcase purple",
		"suitcase panda",
		"suitcase weeb",
		"suitcase guitar"
    };

	private Suitcase currentSuitcase;

    private string lastSuitcaseName = "";

    private int timerValue;

    //Awake is always called before any Start functions
    void Awake()
    {
    	score = 0;
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        InitGame();
    }


    void InitGame()
    {
        this.Moving = true;
        this.Inspection = false;
        this.Timer = false;

        this.inspectCol = GameObject.FindGameObjectsWithTag("InspectCollider")[0].GetComponent<Collider2D>();
        this.reviewCol = GameObject.FindGameObjectsWithTag("ReviewCollider")[0].GetComponent<Collider2D>();

        this.SpawnSuitcase();
    }



    private void SpawnSuitcase()
    {
        // don't let suitcases repeat
        var suitcase = new GameObject();
        var name = "";
        while(name == "" || name == this.lastSuitcaseName)
        {
            name = this.PossibleSuitcases[Random.Range(0, this.PossibleSuitcases.Length)];
        }

        suitcase = Resources.Load("Suitcases/" + name) as GameObject;
        this.lastSuitcaseName = name;
        suitcase.transform.position = new Vector2(-14.0f, 0);

		var go = Instantiate(suitcase);
    }

    public void startInspection(Suitcase suitcase)
    {
		this.currentSuitcase = suitcase;

        this.Moving = false;
        this.Inspection = true;


        var items = new List<GameObject>();

        for (int i = 0; i < suitcase.SpacesCount-1; i++)
        {
            items.Add(Instantiate(Resources.Load("Items/" + this.GoodPossibleItems[Random.Range(0, this.GoodPossibleItems.Length)])) as GameObject);
        }
        
        // we choose 50/50 to add bad item or not
        if (Random.value >= 0.5)
        {
            items.Add(Instantiate(Resources.Load("Items/" + this.BadPossibleItems[Random.Range(0, this.BadPossibleItems.Length)])) as GameObject);
        }
        else
        {
            items.Add(Instantiate(Resources.Load("Items/" + this.GoodPossibleItems[Random.Range(0, this.GoodPossibleItems.Length)])) as GameObject);
        }

        suitcase.FillSuitcase(items);

        var scanGO = GameObject.FindGameObjectsWithTag("ScanLines")[0];
        scanGO.GetComponent<ScanLines>().Drop();
    }

    public void endInspection(bool? result, bool timeout = false)
    {
        this.Moving = true;
        this.Inspection = false;
        this.inspectCol.enabled = false;
		this.currentSuitcase.HideOutlinesInItems();

        this.UI.Ok.gameObject.SetActive(false);
        this.UI.Nope.gameObject.SetActive(false);

        this.currentResult = result;
        this.currentTimeout = timeout;

        if(timeout)
        {
            this.EnableLamp(Color.blue);
        }
    }


    void Update()
    {
        if(LampLightOn)
        {
            this.LampLight.range = Mathf.PingPong(Time.time * 7.0f, 11.0f);
        }
    }

    IEnumerator SmoothUpLamp() {
    	LampLight.range = 1f;
		while(LampLight.range < 11f) {
			LampLight.range += Time.deltaTime * 7f;
			yield return null;
		}
		LampLightOn = true;
		yield break;
	}

	IEnumerator SmoothDownLamp() {
		while(LampLight.range > 0f) {
			LampLight.range -= Time.deltaTime * 3f;
			LampLight.range = Mathf.Clamp(LampLight.range, 0f, 11f);
			yield return null;
		}
		LampLight.enabled = false;
		this.Lamp.color = Color.white;
		yield break;
	}

    public void EnableLamp(Color color)
    {
        this.Lamp.color = color;
        this.LampLight.enabled = true;
        StartCoroutine(SmoothUpLamp());
        LampLightOn = false;
    }

    public void DisableLamp()
    {
        LampLightOn = false;
        StartCoroutine(SmoothDownLamp());
    }

    public void startReview()
    {
        this.Moving = false;
        this.Review = true;
        this.currentSuitcase.OpenSuitcase();

        this.UI.RandomText.text = "";

        //TODO:
        // DO actual review and call end from there

        if(!this.currentTimeout)
        {
            if(this.currentSuitcase.DoIHaveBadItems() == !this.currentResult)
            {
                // add points and all good stuff
                this.EnableLamp(Color.green);
                this.UI.RandomText.enabled = true;
                this.UI.RandomText.text = "pass";
                score += (int)(Random.Range(30f, 60f) * this.timerValue * (this.currentResult != false  ? 1.0f : 1.5f));
                this.UI.Score.text = score.ToString() + " $";
                this.UI.Boss.value -= 0.1f * (this.currentResult != false  ? 1.0f : 1.5f);
                SoundManager._instance.PlaySfx(SoundManager._instance.succ);
            }
            else
            {
                // do bad stuff
                this.EnableLamp(Color.red);
                this.UI.RandomText.enabled = true;
                this.UI.RandomText.text = "fail";
                this.UI.Boss.value += 0.15f * (this.currentResult != false  ? 1.0f : 1.5f);
                SoundManager._instance.PlaySfx(SoundManager._instance.wrong);

                this.currentSuitcase.OpenSuitcase(this.currentResult == true, this.currentResult == false);
            }
        }
        else
        {
            // do bad stuff
            this.UI.Boss.value += 0.2f;
            this.UI.RandomText.enabled = true;
            this.UI.RandomText.text = "why do you even bother?";
            SoundManager._instance.PlaySfx(SoundManager._instance.fail);
        }
        Invoke("resetLamp", 3.0f);      
        Invoke("endReview", 1.0f);      
    }

    // and text    
    public void resetLamp()
    {
        this.DisableLamp();
        this.UI.RandomText.enabled = false;
    }

    public void endReview()
    {
        this.Moving = true;
        this.Review = false;
        this.reviewCol.enabled = false;
    }

    public void startNewCycle()
    {
        this.inspectCol.enabled = true;
        this.reviewCol.enabled = true;
        this.SpawnSuitcase();
    }

    public void InitTimer()
    {
        this.Timer = true;
        this.timerValue = 4;

        this.UI.RandomText.enabled = true;
        this.UI.RandomText.text = "hunt!";

        this.UI.Ok.gameObject.SetActive(true);
        this.UI.Nope.gameObject.SetActive(true);
        Invoke("decreaseTimer", 1.0f);
    }

    public void decreaseTimer()
    {
        this.UI.RandomText.enabled = false;
        if(this.Inspection)
        {
            this.UI.CountDown.enabled = true;
            this.UI.CountDown.text = this.timerValue.ToString();

            if(this.timerValue > 1)
            {
                this.timerValue -= 1;
                Invoke("decreaseTimer", 1.0f);
                this.UI.CountDown.text = this.timerValue.ToString();
            }
            else
            {
                this.UI.CountDown.enabled = false;
                this.UI.RandomText.enabled = true;
                this.UI.RandomText.text = "timeout";
                this.endInspection(null, true);
            }
        }
    }

    public void Ok()
    {
        this.endInspection(true, false);
        this.UI.CountDown.enabled = false;
        this.UI.RandomText.enabled = false;
        SoundManager._instance.PlaySfx(SoundManager._instance.click);
    }

    public void Nope()
    {
        this.endInspection(false, false);
        this.UI.CountDown.enabled = false;
        this.UI.RandomText.enabled = false;
        SoundManager._instance.PlaySfx(SoundManager._instance.click);
    }

    public bool shouldMove()
    {
        // we check state and if we hold mouse it should not move too
        return this.Moving && !Input.GetMouseButton(0);
    }


    /// <summary>
    /// delegated responsibility of win/lose screen loading & and settign some specific values
    /// </summary>
    /// <param name="haveWon">If set to <c>true</c> have won.</param>
    public void GameResultScene(bool haveWon) {
		SceneManager.LoadScene(haveWon ? winSceneName : loseSceneName);
    }

    #if DEBUG && UNITY_EDITOR

    void OnGUI() {
		GUILayout.BeginArea(new Rect(0, Screen.height * 14 / 15, Screen.width, Screen.height / 15));

		GUILayout.BeginHorizontal();
		if(GUILayout.Button("add score Random[100, 250]")) {
			score += Random.Range(100, 250);
			// update visals
			UI.Score.text = score + " $";
		}
		if(GUILayout.Button("insta win")) {
			GameResultScene(true);
		}
		if(GUILayout.Button("insta lose")) {
			GameResultScene(false);
		}
    	GUILayout.EndHorizontal();

    	GUILayout.EndArea();

    }
    #endif
}