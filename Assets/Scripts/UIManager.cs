﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	[Header("Not directly interactible")]
	public Text CountDown;
	public Text RandomText;
	public Button Ok;
	public Button Nope;
	public Text Score;

	[Header("Boss stuff")]
	public Slider Boss;

	public Sprite BossAngry;
	public Sprite BossNeutral;
	public Sprite BossHappy;

	public Image BossHandle;

	[Header("PauseMenu stuffs")]
	public Canvas pauseCanvas;
	public Canvas explainCanvas;

	bool considerGameplay = true;
	bool inPauseMenu = false;
	static bool explanationsShown = false;
	bool inExplainMenu = false;

	// Use this for initialization
	void Start() {
		this.Ok.onClick.AddListener(() => { GameManager.instance.Ok(); });
		this.Nope.onClick.AddListener(() => { GameManager.instance.Nope(); });
		considerGameplay = true;
		inPauseMenu = false;
		inExplainMenu = false;
		if(!explanationsShown) {
			ShowExplainMenu();
		}
	}
	
	// Update is called once per frame
	void Update() {
		if(Input.GetKeyDown(KeyCode.Escape)) {
			ShowPauseMenu();
		}
		if(!considerGameplay) {
			return;
		}

		if(this.Boss.value <= 0.2) {
			this.BossHandle.sprite = this.BossHappy;
		} else if(this.Boss.value < 0.8) {
			this.BossHandle.sprite = this.BossNeutral;
		} else {
			this.BossHandle.sprite = this.BossAngry;
		}
		if(this.Boss.value < 0.1) {
			considerGameplay = false;
			GameManager.instance.GameResultScene(true);
			return;
		}
		if(this.Boss.value > 0.9) {
			considerGameplay = false;
			GameManager.instance.GameResultScene(false);
			return;
		}
	}

	public void ShowPauseMenu(bool fromHardware = false) {
		if(inExplainMenu && !fromHardware) {
			HideExplainMenu();
			return;
		}
		if(inPauseMenu && !fromHardware) {
			HidePauseMenu();
			return;
		}
		pauseCanvas.gameObject.SetActive(true);
		inPauseMenu = true;
		Time.timeScale = 0f;
	}

	public void HidePauseMenu() {
		pauseCanvas.gameObject.SetActive(false);
		inPauseMenu = false;
		Time.timeScale = 1f;
	}

	public void ShowExplainMenu() {
		explanationsShown = true;
		inExplainMenu = true;
		explainCanvas.gameObject.SetActive(true);
		Time.timeScale = 0f;
	}

	public void HideExplainMenu() {
		explainCanvas.gameObject.SetActive(false);
		inExplainMenu = false;
		if(!pauseCanvas.gameObject.activeSelf) {
			Time.timeScale = 1f;
		}
	}


	/// <summary>
	/// returns true, when getting paused; false, when coming back from background
	/// </summary>
	/// <param name="paused">If set to <c>true</c> paused.</param>
	void OnApplicationPause(bool paused) {
		if(paused) {
			ShowPauseMenu(true);
		}
	}

}
