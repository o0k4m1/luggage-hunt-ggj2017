﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Item : MonoBehaviour {
	public bool Bad = false;
	public string Category;

	public SpriteRenderer Base;
	public SpriteRenderer Outline;

	[SerializeField]
	private Sprite baseSprite;
	[SerializeField]
	private Sprite outlineSprite;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Reveal()
	{
		this.Base.enabled = true;
		this.Outline.enabled = false;
	}

	public void HideAll()
	{
		this.Base.enabled = false;
		this.Outline.enabled = false;
	}

	public void OutlineBad()
	{
		if(this.Bad)
		{
			this.Base.enabled = true;
			this.Outline.enabled = true;
			this.Outline.color = Color.red;
		}
	}

		public void OutlineGood()
	{
		if(!this.Bad)
		{
			this.Base.enabled = true;
			this.Outline.enabled = true;
			this.Outline.color = Color.green;
		}
	}

	#if UNITY_EDITOR
	[ContextMenu("Assign Visual Stuffs")]
	public void ASsignSR() {
		var outline = gameObject.transform.Find("outline");
		Outline = outline.GetComponentInChildren<SpriteRenderer>();
		var _base = gameObject.transform.Find("base");
		Base = _base.GetComponentInChildren<SpriteRenderer>();
		Undo.RecordObjects(new Object[] {this, Outline, Base}, "auto set sprites for " + name);
		Base.enabled = false;
		if(outlineSprite != null) {
			Outline.sprite = outlineSprite;
			outlineSprite = null;
		}
		if(baseSprite != null) {
			Base.sprite = baseSprite;
			baseSprite = null;
		}

		var col = Base.gameObject.AddComponent<BoxCollider2D>();

		var currcol = GetComponent<BoxCollider2D>();
		currcol.offset = col.offset;
		currcol.size = col.size;

		DestroyImmediate(col);
	}


	#endif
}
