﻿using SingletonPattern;
using UnityEngine;

public class SoundManager : Singleton<SoundManager> {
	//declare our public and private variables
	public AudioSource efxSource;                    
	public AudioSource musicSource;                       
	public float lowPitchRange = .95f;
	public float highPitchRange = 1.05f;

	//we'll create a global audio listener
	public AudioListener globalAudioListener;
	
	//we use an enum here to handle the various Sound States
	// public enum SoundState { MainMenu };
	// public static SoundState soundState;

	//create a public instance of the SoundManager object 
	//so we can access it from other places
	public static SoundManager _instance;

	//create a boolean so that we can manage the state of the music
	//we want to know if its already playing or not. 
	public static bool isPlaying = false;
	
	// it would be a good idea to create lists of audio clips when you have a lot.
	//scene music
	public AudioClip mainMusic;
	
	//sfx
	public AudioClip scan;
	public AudioClip click;
	public AudioClip succ;
	public AudioClip fail;
	public AudioClip wrong;

	public AudioClip secretMusic;
	
	void Awake() {
		//sets soundManager to THIS instance of the SoundManager      
		if(_instance != null)
		{
			Destroy(gameObject);
			return;
		}
		else
		{
			_instance = this;
		}

		//in unity you should only have 1 AudioListener in a scene
		//and it needs to be in the scene with the managers
		//for this reason we'll prevent destroying it between scenes  
		DontDestroyOnLoad(globalAudioListener);
	}
	
	// here we check update what the current SoundState is and play the correct music // for the correct state.
	void Update() {
		if (isPlaying == false) {
			// if(soundState == SoundState.MainMenu) {
				isPlaying = true;
				SceneMusic(mainMusic);
			// }
		}
	}
	
	//play sfx
	public void PlaySfx(AudioClip clip) {
		efxSource.clip = clip;
		efxSource.Play ();
	}
	
	//play randomize repetitive sound effects
	public void RandomSfx(params AudioClip[] clips) {
		int randomIndex = Random.Range(0, clips.Length);
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);
		efxSource.pitch = randomPitch;
		efxSource.clip = clips[randomIndex];
		efxSource.Play();
	}

	// change scene music
	public void SceneMusic(AudioClip clip) {
		musicSource.clip = clip;
		musicSource.Play();
	}

	public void ToggleMusic()
	{
		this.musicSource.mute = !this.musicSource.mute;
	}

	public void ToggleSound()
	{
		this.efxSource.mute = !this.efxSource.mute;
	}

	public void SwitchMusic()
	{
		if(musicSource.clip == this.mainMusic)
		{
			SceneMusic(secretMusic);
		}
		else
		{
			SceneMusic(mainMusic);
		}
	}
}