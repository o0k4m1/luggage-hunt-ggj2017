﻿using System.Collections.Generic;
using UnityEngine;

public class Suitcase : MonoBehaviour {
	Rigidbody2D rb;
	Collider2D col;
	private float MoveForce = 130.0f;
	public bool Inspected;

	public SpriteRenderer Open;
	public SpriteRenderer Closed;

	public Collider2D currentCollider;
	Vector3 lastPosition;

	private float minimalDistance = 3.5f;

	// min 1!
	public int SpacesCount = 3;


	void Start () {
		this.rb = GetComponent<Rigidbody2D>();
		this.col = GetComponent<Collider2D>();
		lastPosition = transform.localPosition;
	}

	void Update() {
		// if(Time.deltaTime > 0.05) {
		// 	Debug.LogFormat("lag spike: {0}", Time.deltaTime);
		// }
		if(GameManager.instance.shouldMove()) {
			// strange, why is this even happening? is it because physics stuff are applied
			// in fixed update and Update have to be active for UI responses?
			if(!Mathf.Approximately(Time.timeScale, 0f)) {
        		this.rb.AddForce(Vector2.right * this.MoveForce, ForceMode2D.Force);
			}
		}
		else
		{
			this.rb.velocity = Vector3.zero;
		}
		var modeDiff = transform.localPosition - lastPosition;
		Laner.Instance.MoveLanesBy(modeDiff.x);
		lastPosition = transform.position;
    }
	
	void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "InspectCollider")
        {
			Debug.Log("start inspection");
			GameManager.instance.startInspection(this);
		}
		else if (coll.gameObject.tag == "ReviewCollider")
        {
			Debug.Log("start review");
			GameManager.instance.startReview();
		}
		else if (coll.gameObject.tag == "DestroyCollider")
        {
			Debug.Log("destroy suitcase!");
			GameManager.instance.startNewCycle();
			Destroy(gameObject);
		}
    }

	public void FillSuitcase(IList<GameObject> items)
	{
		var existingPositions = new List<Vector2>();
		foreach (var item in items)
		{
			item.transform.SetParent(gameObject.transform);
			
			// rotate
			var randomRotation = Random.rotation;
			var rotQuat = Quaternion.Euler(0f, 0f, randomRotation.eulerAngles.z);
			item.transform.localRotation *= rotQuat;			

			// find cool place in suitcase
			var newPos = this.FindPositionForNewItem(this.col.bounds, existingPositions, item);

			item.transform.position = newPos;
			existingPositions.Add(newPos);
		}
	}

	private Vector2 FindPositionForNewItem(Bounds suitcaseBounds, IList<Vector2> existingPositions, GameObject item)
	{
		Debug.Log(existingPositions.Count);
		var currentMinimal = minimalDistance;

		var sampleVector = new Vector2();
		var found = false;
		var tryCount = 0;
		var itemBounds = item.GetComponent<BoxCollider2D>().bounds;
		var minX = suitcaseBounds.min.x + itemBounds.extents.x;
		var maxX = suitcaseBounds.max.x - itemBounds.extents.x;
		var minY = suitcaseBounds.min.y + itemBounds.extents.y;
		var maxY = suitcaseBounds.max.y - itemBounds.extents.y;

		// we try 200 times to find suitable random point
		while(!found && tryCount < 200)
		{
			sampleVector = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
			var distanceToCenter = Vector2.Distance(sampleVector, suitcaseBounds.center);

			// questionable stuff
			bool firstTooCloseToCenter = (existingPositions.Count == 0) && (distanceToCenter < 1.0f);

			var allGood = true;

			foreach(var pos in existingPositions)
			{
				var distance = Vector2.Distance(pos, sampleVector);

				// if distance too short
				if(distance < minimalDistance)
				{
					allGood = false;
				}
			}

			found = allGood && !firstTooCloseToCenter;

			if(!found)
			{
				currentMinimal = currentMinimal - (minimalDistance / 200.0f);
			}

			tryCount++;
		}

		// Debug.Log(currentMinimal + " minimal");

		return sampleVector;
	}

	public void HideOutlinesInItems()
	{
		foreach(var item in gameObject.GetComponentsInChildren<Item>()) {
			item.HideAll();
		};
	}

	public void OpenSuitcase(bool showBadItems = false, bool showGoodItems = false)
	{
		this.Open.enabled = true;
		this.Closed.enabled = false;

		foreach(var item in gameObject.GetComponentsInChildren<Item>()) {
			item.Reveal();
			
			if(showBadItems)
			{
				item.OutlineBad();
			}
			if(showGoodItems)
			{
				item.OutlineGood();
			}
		};
	}

	public bool DoIHaveBadItems()
	{
		var result = false;
		foreach(var item in gameObject.GetComponentsInChildren<Item>()) {
			if(item.Bad)
			{
				result = true;
			}
		};
		return result;
	}
}
