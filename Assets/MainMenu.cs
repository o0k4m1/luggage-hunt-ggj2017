﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	void Start()
	{
		this.FixButtons();
	}

	public Button Music;
	public Button Sound;

	public Sprite MusicEnabled;
	public Sprite MusicDisabled;
	public Sprite SoundEnabled;
	public Sprite SoundDisabled;

	public void ToggleSound()
	{
		SoundManager._instance.ToggleSound();
		this.FixButtons();
	}

	public void ToggleMusic()
	{
		SoundManager._instance.ToggleMusic();
		this.FixButtons();
	}

	public void FixButtons()
	{
		Sound.image.sprite = SoundManager._instance.efxSource.mute ?
			this.SoundDisabled : this.SoundEnabled;
		Music.image.sprite = SoundManager._instance.musicSource.mute ?
			this.MusicDisabled : this.MusicEnabled;

	}

	public void Secret()
	{
		SoundManager._instance.SwitchMusic();
	}
}
